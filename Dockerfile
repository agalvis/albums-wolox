FROM maven:3.6.2-jdk-8
WORKDIR app
COPY /target/albums-0.0.1-SNAPSHOT.jar ./
EXPOSE 8080
CMD ["java", "-jar", "albums-0.0.1-SNAPSHOT.jar"]