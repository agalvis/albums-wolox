# Proyecto Albums Wolox Spring Boot __Version 2.3.5.RELEASE__

Este proyecto concentra información de una API
externa para luego poder extenderla a una nueva funcionalidad,
la cual consiste en implementar una gestión de permisos básica
a cada albúm de fotos (lectura y escritura) para compartir 
álbumes entre los usuarios de la plataforma.

## Pre-requisitos

1. Tener instalado GIT en su computadora
2. Contar con Java JDK 8 como minimo
3. Soportar H2
4. Tener permisos en el repositorio [Albums-wolox](https://gitlab.com/agalvis/albums-wolox.git)
5. Opcional tener instalado docker

## Instalación

Para obtener una copia de este proyecto ejecute el siguiente comando.

```bash
git clone https://gitlab.com/agalvis/albums-wolox.git
```
___
# Uso

Lo primero que debes conocer es el uso del archivo __application.yml__, el cual es usado para las configuraciones mas relevantes del proyecto.

> Ubicación del archivo: \src\main\resources\application.yml

## Sección Server

En esta sección podras ver la configuración del servidor, donde se le indica el puerto de salida __```port: 8080```__, al igual que el path inicial __```servlet-path: /v1.0/```__ al iniciar la aplicacion podra acceder a ella desde su navegador ingresando a la url __http://localhost:8080/v1.0/__ .

```yaml
server:
	port: 8080
	servlet:
		context-path: /v1.0/
```

### 1. Swagger

En este apartado podra activar o desactivar swagger, solo debe indicar el valor __true ó false__ en el campo __enabled__.

```yaml
albums:
  swagger:
    enabled: true
    version: "v1.0"
    title: "Application Micro Service"
    base-package: "co.com.wolox.albums"
    description: "Microservicios"
    license: "Alexander Galvis"
    contact:
      name: "Alexander Galvis"
      email: "galcorp@gmail.com"
  ...
```

Para acceder a swagger desde el navegador debera ingresar al link (http://localhost:8080/v1.0/swagger-ui.html) o a la url creada en el servidor donde es ejecutado el proyecto y deberas ver algo parecido a la siguiente imagen.

![swagger-ui](https://i.imgur.com/EZDMoyC.png)

### 2. Security

En este apartado podras activar o desactivar la seguridad de los api endpoints y agregar las configuraciones del JWT.


```yaml
albums:
  ...
  security:
    enabled: true
    timezone: "America/Bogota"
    token:
      secret: "SECRET_KEY"
      expires-in: 3600 # seconds
    excluded:
      path: "/v1.0/example1,/v1.0/example2"
  ...
```

__- Activar/Desactivar:__ Para activar o desactivar la seguridad, solo debe cambiar el _valor_ del campo enabled a _***true o false***_, segun lo requiera.

> Al activar la seguridad en la aplicación, debera ver el boton de __Authorize__ en swagger.

![swagger-ui-security](https://i.imgur.com/Zpis93X.png)

***- Autenticación:*** Para autenticarse en el sistema debera enviar los valores __client_id__ y __client_secret__ a la url http://localhost:8080/v1.0/oauth/login el cual solo esta disponible si el valor del campo enabled es true.

Ejemplo de petición:


***[POST]***
http://localhost:8080/v1.0/oauth/login

***[Request]***

_body type:_ 
***x-www-form-urlencoded***

| Key | Value |
| --------------- | --------------- |
| client_id | "string" |
| client_secret | "string" |

***[Response]***
```
{
	"token_type": "bearer",
	"issued_at": number,
	"client_id": "string",
	"access_token": "string",
	"expires_in": number
}
```
---
## Excepciones

Para la captura de excepciones se cuenta con la utilidad ***ApiException.java*** la cual cuenta con la configuración soportada para las excepciones en ```runtime```.

#### Configuración de mensajes de error

Los mensajes de error son configurados en la clase de tipo ```enum``` ***ErrorEnum.java*** la cual se encuentra en ***co.com.wolox.albums.enums*** tal como se muestra a continuación:

```java
AUTH_GRANTYPE_INVALID("El grant_type es invalido", HttpStatus.INTERNAL_SERVER_ERROR)
```

#### Ejemplo de captura de error

```java
throw new ApiException(ErrorEnum.AUTH_GRANTYPE_INVALID);
```

[Response]
```json
{
"code": "string",
"description": "string",
"status": "string"
}
```

---
## Despliegue

Para desplegar la aplicación siga los siguientes comandos.

```bash
mvn clean install
java -jar <PATH_PROJECT>/<NAME_PROJECT>-0.0.1-SNAPSHOT.jar
```
---

Docker

```bash
mvn package
docker build -t album_wolox . && docker run -p 8080:8080 --name album_wolox album_wolox
```
---
## Autor


* ***Alexander Galvis***


