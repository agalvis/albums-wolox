package co.com.wolox.albums.clients;

import co.com.wolox.albums.dto.generated.Albums;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "${client.resources.albums}", url = "${client.api.url}")
public interface AlbumsClientRest {

    /**
     * consulta los albunes asociados a un usuario
     *
     * @param id
     * @return
     */
    @GetMapping(value = "${client.resources.users}/{id}/${client.resources.albums}")
    List<Albums> albumsByUserId(@PathVariable("id") Integer id);

    /**
     * Cconsulta un albun por ID
     * @param id
     * @return
     */

    @GetMapping(value = "${client.resources.albums}/{id}")
    Albums albumsById(@PathVariable("id") Integer id);
}

