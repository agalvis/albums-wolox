package co.com.wolox.albums.clients;

import co.com.wolox.albums.dto.generated.Comments;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${client.resources.comments}", url = "${client.api.url}")
public interface CommentsClientRest {

    /**
     * Consulta comentarios por el campo name
     * @param name
     * @return
     */
    @GetMapping(value = "${client.resources.comments}")
    List<Comments> commentsByName(@RequestParam(value = "name") String name);


    /**
     * Consulta los comentarios por postID
     * @param postId
     * @return
     */
    @GetMapping(value = "${client.resources.comments}")
    List<Comments> commentsByPostId(@RequestParam(value = "postId") Integer postId);
}
