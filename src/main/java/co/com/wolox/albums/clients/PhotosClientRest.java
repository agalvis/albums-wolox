package co.com.wolox.albums.clients;

import co.com.wolox.albums.dto.generated.Photos;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "${client.resources.photos}", url = "${client.api.url}")
public interface PhotosClientRest {

    /**
     * Consulta todas las fotos registradas
     * @return List<Photos>
     */
    @GetMapping(value = "${client.resources.photos}")
    List<Photos> photos();

    /**
     * Consulta las fotos por id
     * @param id
     * @return List<Photos>
     */
    @GetMapping(value = "${client.resources.albums}/{id}/${client.resources.photos}")
    List<Photos> photosByAlbumId(@PathVariable("id") Integer id);

}
