package co.com.wolox.albums.clients;

import co.com.wolox.albums.dto.generated.Posts;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "${client.resources.post}", url = "${client.api.url}")
public interface PostClienteRest {

    /**
     * Consulta todos los posts
     * @return List<Posts>
     */
    @GetMapping(value = "${client.resources.post}")
    List<Posts> posts();

    /**
     * Consulta todos los posts por userId
     * @param idUser
     * @return List<Posts>
     */
    @GetMapping(value = "${client.resources.post}")
    List<Posts> postsByIdUser(@RequestParam(value = "userId") Long idUser);

}
