package co.com.wolox.albums.clients;

import co.com.wolox.albums.dto.generated.Users;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "${client.resources.users}", url = "${client.api.url}")
public interface UserClientRest {

    /**
     * Consulta todos los usuarios
     * @return List<Users>
     */
    @GetMapping(value = "${client.resources.users}")
    List<Users> users();

    /**
     * Consulta un usuario por ID
     * @param userId
     * @return List<Users>
     */
    @GetMapping(value = "${client.resources.users}/{userId}")
    Users usersByUserId(@PathVariable(value = "userId") Integer userId);

}
