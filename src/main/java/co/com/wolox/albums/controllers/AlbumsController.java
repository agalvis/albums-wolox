package co.com.wolox.albums.controllers;

import co.com.wolox.albums.dto.generated.Albums;
import co.com.wolox.albums.exceptions.ApiErrorResponse;
import co.com.wolox.albums.services.AlbumsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@ApiOperation(value = "Retorna los datos de los albunes")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = Albums[].class),
        @ApiResponse(code = 401, message = "Unauthorized", response = ApiErrorResponse[].class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ApiErrorResponse[].class)
})
@RequestMapping("/album")
public class AlbumsController {

    @Autowired
    private AlbumsService albumsService;

    @GetMapping(value = "${client.resources.users}/{userId}/${client.resources.albums}")
    public ResponseEntity<List<Albums>> albumsByUserId(
            @ApiParam(value = "id del usuario", required = true)
            @PathVariable(value = "userId") Integer userId) {
        return ResponseEntity.ok(albumsService.albumsByUserId(userId));
    }
}
