package co.com.wolox.albums.controllers;

import co.com.wolox.albums.enums.ErrorEnum;
import co.com.wolox.albums.exceptions.ApiErrorResponse;
import co.com.wolox.albums.exceptions.ApiException;
import co.com.wolox.albums.security.dto.JwtResponse;
import co.com.wolox.albums.security.service.LoginService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controlador para la autenticacion de usuarios
 */
@RestController
@RequestMapping(path = "")
@ApiOperation(value = "Retorna los datos de autenticación")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = JwtResponse.class),
        @ApiResponse(code = 400, message = "Error en mensaje de entrada", response = ApiErrorResponse.class),
        @ApiResponse(code = 401, message = "Código invalido de autorización", response = ApiErrorResponse.class)
})
@ConditionalOnProperty(value = "albums.security.enabled", havingValue = "true", matchIfMissing = true)
public class AuthController {

    @Autowired
    private LoginService loginService;

    /**
     * Retorna los datos de autenticacion
     * @param paramMap
     * @return ResponseEntity<JwtResponse>
     */
    @PostMapping(path = "oauth/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JwtResponse> login(
            @ApiParam(value = "Información de autenticación : paramMap {client_id: username, client_secret: secret}", required = true)
            @RequestBody MultiValueMap<String, String> paramMap) {

        if (paramMap == null ||
                (paramMap.getFirst("client_id") ==null || paramMap.getFirst("client_id").isEmpty()) ||
                (paramMap.getFirst("client_secret") ==null ||paramMap.getFirst("client_secret").isEmpty())
        ) {
            throw new ApiException(ErrorEnum.AUTH_CREDENTIALS_INVALID);
        }

        return ResponseEntity.ok(this.loginService.login(
                paramMap.getFirst("client_id"),
                paramMap.getFirst("client_secret")));

    }

}
