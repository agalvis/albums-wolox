package co.com.wolox.albums.controllers;

import co.com.wolox.albums.dto.generated.Comments;
import co.com.wolox.albums.exceptions.ApiErrorResponse;
import co.com.wolox.albums.services.CommentsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@ApiOperation(value = "Retorna los datos de los comentarios")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = Comments[].class),
        @ApiResponse(code = 401, message = "Unauthorized", response = ApiErrorResponse[].class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ApiErrorResponse[].class)
})
@RequestMapping("/album")
public class CommentsController {

    @Autowired
    private CommentsService commentsService;

    @GetMapping(value = "${client.resources.comments}")
    public ResponseEntity<List<Comments>> commentsByName(
            @ApiParam(value = "campo name del comentario", required = true)
            @RequestParam(value = "name") String name) {
        return ResponseEntity.ok(commentsService.commentsByName(name));
    }

    @GetMapping(value = "${client.resources.comments}/{idUser}/${client.resources.users}")
    public ResponseEntity<List<Comments>> commentsByIdUser(
            @ApiParam(value = "id del usuario", required = true)
            @PathVariable(value = "idUser") Long idUser) {
        return ResponseEntity.ok(commentsService.commentsByIdUser(idUser));
    }
}
