package co.com.wolox.albums.controllers;

import co.com.wolox.albums.dto.ShareReq;
import co.com.wolox.albums.dto.ShareRes;
import co.com.wolox.albums.exceptions.ApiErrorResponse;
import co.com.wolox.albums.models.Share;
import co.com.wolox.albums.services.ShareService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@ApiOperation(value = "Retorna los datos compartidos")
@ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK", response = ShareRes[].class),
        @ApiResponse(code = 201, message = "Created", response = Share.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = ApiErrorResponse[].class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = ApiErrorResponse[].class)
})
@RequestMapping("/album")
public class ShareController {

    @Autowired
    private ShareService shareService;

    @PostMapping(value = "${client.resources.share}")
    @ResponseStatus(HttpStatus.CREATED)
    public ShareRes save(
            @ApiParam(value = "Objeto con el album a compartir", required = true)
            @RequestBody ShareReq shareDTO){
        return shareService.save(shareDTO);
    }

    @PutMapping(value = "${client.resources.share}")
    @ResponseStatus(HttpStatus.CREATED)
    public ShareRes update(
            @ApiParam(value = "Objeto con el album compartido", required = true)
            @RequestBody ShareReq shareDTO){
        return shareService.update(shareDTO);
    }
}
