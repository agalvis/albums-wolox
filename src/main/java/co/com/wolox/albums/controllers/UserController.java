package co.com.wolox.albums.controllers;

import co.com.wolox.albums.dto.UsersShareRes;
import co.com.wolox.albums.dto.generated.Users;
import co.com.wolox.albums.enums.Permission;
import co.com.wolox.albums.exceptions.ApiErrorResponse;
import co.com.wolox.albums.services.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@ApiOperation(value = "Retorna los datos del usuario")
@RequestMapping("/album")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Users[].class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ApiErrorResponse[].class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ApiErrorResponse[].class)
    })
    @GetMapping(value = "${client.resources.users}")
    public ResponseEntity<List<Users>> users() {
        return ResponseEntity.ok(userService.findAll());
    }

    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = UsersShareRes[].class),
            @ApiResponse(code = 401, message = "Unauthorized", response = ApiErrorResponse[].class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ApiErrorResponse[].class)
    })
    @GetMapping(value = "${client.resources.users}/{permission}/{idAlbum}/${client.resources.albums}")
    public ResponseEntity<UsersShareRes> usersByPermissionAlbum(
            @ApiParam(value = "Campo con el permiso a buscar", required = true)
            @PathVariable(value = "permission") Permission permission,
            @ApiParam(value = "Id del albun", required = true)
            @PathVariable(value = "idAlbum") Long idAlbum) {
        return ResponseEntity.ok(userService.usersByPermissionAlbum(permission, idAlbum));
    }
}
