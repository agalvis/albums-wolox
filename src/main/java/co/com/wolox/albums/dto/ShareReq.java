package co.com.wolox.albums.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Clase request de Share
 */
@AllArgsConstructor
public @Data class ShareReq {
    private Long id;
    private Long idUser;
    private Long idAlbum;
    private String permission;
}
