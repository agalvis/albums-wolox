package co.com.wolox.albums.dto;

import co.com.wolox.albums.dto.generated.Albums;
import co.com.wolox.albums.dto.generated.Users;
import co.com.wolox.albums.enums.Permission;
import lombok.Data;

/**
 * Clase response de los Share
 */
public @Data class ShareRes {
    private Long id;
    private Users users;
    private Albums albums;
    private Permission permission;

    public ShareRes(Long id, Users users, Albums albums, Permission permission) {
        this.id = id;
        this.users = users;
        this.albums = albums;
        this.permission = permission;
    }
}
