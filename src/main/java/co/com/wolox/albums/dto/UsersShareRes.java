package co.com.wolox.albums.dto;

import co.com.wolox.albums.dto.generated.Albums;
import co.com.wolox.albums.dto.generated.Users;
import co.com.wolox.albums.enums.Permission;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Clase response con los usuarios que comparten un albun
 */
@AllArgsConstructor
public @Data class UsersShareRes {
    private Albums album;
    private Permission permission;
    private List<Users> users;

    public UsersShareRes() {
    }
}
