package co.com.wolox.albums.enums;

import org.springframework.http.HttpStatus;

/**
 * Lista de errores que tendra la aplicacion
 */
public enum ErrorEnum {

    BAD_REQUEST("Solicitud incorrecta", HttpStatus.BAD_REQUEST),
    BAD_REQUEST_SHARE("Usuario y/o Album no valido", HttpStatus.BAD_REQUEST),
    BAD_REQUEST_UNIQUE("Album ya registrado", HttpStatus.OK),
    BAD_REQUEST_NOT_REGISTERED("Usuario y/o Album no registrado", HttpStatus.BAD_REQUEST),
    AUTH_CREDENTIALS_INVALID("El client_id y/o client_secret es invalido", HttpStatus.UNAUTHORIZED),
    BAD_TOKEN("No autorizado", HttpStatus.UNAUTHORIZED);


    private String description;
    private HttpStatus httpCode;

    private ErrorEnum(String description, HttpStatus httpCode) {
        this.description = description;
        this.httpCode = httpCode;
    }

    public String getDescription() {
        return description;
    }

    public HttpStatus getHttpCode() {
        return httpCode;
    }

}
