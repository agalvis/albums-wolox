package co.com.wolox.albums.enums;

import lombok.Getter;

/**
 * Enum con los permisos pertimitidos
 */
public enum Permission {
    READ("r"), WRITE("w"), READ_WRITE("rw");

    @Getter
    private String permission;

    Permission(String permission) {
        this.permission = permission;
    }
}
