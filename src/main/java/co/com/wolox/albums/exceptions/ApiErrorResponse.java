package co.com.wolox.albums.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiErrorResponse {
  
  private Integer code;
  private String description;
  private HttpStatus status;

    /**
   * GEt status
   * @return
   */
  public HttpStatus getStatus() {
    status = code != null ? HttpStatus.valueOf(code) : null;
    return status;
  }
}
