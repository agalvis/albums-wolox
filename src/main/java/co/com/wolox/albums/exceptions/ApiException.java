package co.com.wolox.albums.exceptions;

import co.com.wolox.albums.enums.ErrorEnum;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiException extends RuntimeException {

    private ErrorEnum error;
    private String message;
    private ApiErrorResponse response;

    public ApiException(ErrorEnum error) {
        super();
        this.error = error;
    }


    /**
     *
     */
    private static final long serialVersionUID = 1L;
}
