package co.com.wolox.albums.exceptions;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Clase que controla la respuesta de las excepciones
 */
@ControllerAdvice
@AllArgsConstructor
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class GlobalControllerAdvice {

  @ExceptionHandler(ApiException.class)
  public ResponseEntity<List<ApiErrorResponse>> handleApiException(ApiException ex) {

    ResponseEntity response;

    if(ex.getResponse() != null) {
      response = new ResponseEntity(ex.getResponse(), ex.getResponse().getStatus());
    }else {
      ApiErrorResponse r = new ApiErrorResponse(ex.getError().getHttpCode().value(),
              ex.getError().getDescription(), ex.getError().getHttpCode());

      response = new ResponseEntity(r, r.getStatus());
    }
    return response;
  }

}
