package co.com.wolox.albums.models;

import co.com.wolox.albums.enums.Permission;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class PermissionConverter implements AttributeConverter<Permission, String> {

    @Override
    public String convertToDatabaseColumn(Permission category) {
        if (category == null) {
            return null;
        }
        return category.getPermission();
    }

    @Override
    public Permission convertToEntityAttribute(String permission) {
        if (permission == null) {
            return null;
        }

        return Stream.of(Permission.values())
                .filter(c -> c.getPermission().equals(permission))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
