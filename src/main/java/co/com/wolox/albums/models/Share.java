package co.com.wolox.albums.models;

import co.com.wolox.albums.enums.Permission;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
public @Data
class Share implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_USER")
    private Long idUser;
    @Column(name = "ID_ALBUM")
    private Long idAlbum;
    private Permission permission;

    public Share() {
    }

    public Share(Long idAlbum, Permission permission) {
        this.idAlbum = idAlbum;
        this.permission = permission;
    }

    public Share(Long idUser, Long idAlbum, Permission permission) {
        this.idUser = idUser;
        this.idAlbum = idAlbum;
        this.permission = permission;
    }
}
