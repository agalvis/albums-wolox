package co.com.wolox.albums.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@AllArgsConstructor
public @Data class Users implements Serializable {

    private static final long serialVersionUID = 1L;

    public Users() {
    }

    @Id
    @GeneratedValue
    @Basic(optional = false)
    @Column(name = "ID_USER")
    private Long idUser;
    @Column(name = "USER_NAME")
    private String userName;
    @Column(name = "PASSWORD")
    private String password;
}
