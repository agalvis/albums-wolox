package co.com.wolox.albums.models.repositories;

import co.com.wolox.albums.enums.Permission;
import co.com.wolox.albums.models.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {

    Share findByIdUser(Long idUser);

    Share findByIdUserAndIdAlbum(Long idUser, Long idAlbum);

    List<Share> findByIdAlbumAndPermission(Long idAlbum, Permission permission);

}
