package co.com.wolox.albums.models.repositories;

import co.com.wolox.albums.models.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<Users, Long> {

    Users findByUserName(String userName);
}
