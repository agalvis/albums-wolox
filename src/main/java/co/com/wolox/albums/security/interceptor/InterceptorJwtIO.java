package co.com.wolox.albums.security.interceptor;

import co.com.wolox.albums.enums.ErrorEnum;
import co.com.wolox.albums.exceptions.ApiException;
import co.com.wolox.albums.security.jwt.JwtIO;
import io.fusionauth.jwt.InvalidJWTException;
import io.fusionauth.jwt.InvalidJWTSignatureException;
import io.fusionauth.jwt.JWTExpiredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class InterceptorJwtIO implements HandlerInterceptor {

    private static final String AUTH_PATH = "/oauth/login";
    @Value("#{'${albums.security.excluded.path:#}'.split(',')}")
    private List<String> excluded;
    @Value("${albums.swagger.enabled:false}")
    private boolean securityEnabled;

    private static final String AUTHORIZATION = "Authorization";

    @Autowired
    private JwtIO jwtUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {


        boolean validate = false;
        try {

            String uri = request.getRequestURI();

            if (uri.contains(AUTH_PATH) || excluded(uri)) {
                validate = true;
            }

            if (!validate && request.getHeader(AUTHORIZATION) != null &&
                    !request.getHeader(AUTHORIZATION).isEmpty()) {

                String token = request.getHeader(AUTHORIZATION).replace("Bearer ", "");

                validate = !jwtUtil.validateToken(token);
            }

            if (!validate) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } catch (InvalidJWTException | InvalidJWTSignatureException | JWTExpiredException e) {
            throw new ApiException(ErrorEnum.BAD_TOKEN);
        }

        return validate;
    }

    private boolean excluded(String path) {

        boolean result = false;

        if (securityEnabled) {

            excluded.add("swagger-ui.html");
            excluded.add("webjars");
            excluded.add("swagger-resources");
        }

        for (String exc : excluded) {

            if (path != null && exc !=null && path.contains(exc)) {
                result = true;
            }
        }
        return result;
    }
}
