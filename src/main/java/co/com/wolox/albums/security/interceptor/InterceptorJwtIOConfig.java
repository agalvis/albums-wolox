package co.com.wolox.albums.security.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Component
public class InterceptorJwtIOConfig implements WebMvcConfigurer {

	@Value("${albums.security.enabled:false}")
	private boolean securityEnabled;
	@Value("${albums.swagger.enabled}")
	private boolean swaggerEnabled;

	@Autowired
	private InterceptorJwtIO interceptorUtil;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		if (securityEnabled) {
			registry.addInterceptor(interceptorUtil);
		}
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		
		if (swaggerEnabled) {

			registry.addResourceHandler("/v1.0/swagger-ui.html")
			.addResourceLocations("classpath:/META-INF/resources/");
			registry.addResourceHandler("/webjars/**")
			.addResourceLocations("classpath:/META-INF/resources/webjars/");
		}
	}

}
