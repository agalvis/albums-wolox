package co.com.wolox.albums.security.jwt;

import co.com.wolox.albums.utils.GsonUtils;
import io.fusionauth.jwt.Signer;
import io.fusionauth.jwt.Verifier;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.hmac.HMACSigner;
import io.fusionauth.jwt.hmac.HMACVerifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.TimeZone;

@Component
public class JwtIO {

    @Value("${albums.security.token.secret:secret}")
    private String SECRET;
    @Value("${albums.security.timezone:UTC}")
    private String TIMEZONE;
    @Value("${albums.security.token.expires-in:3600}")
    private int EXPIRE_IN;
    @Value("${albums.security.issuer:none}")
    private String ISSUER;

    /**
     * Retorna el access_token firmado
     *
     * @param src
     * @return
     */
    public String generateToken(Object src) {

        String subject = GsonUtils.serialize(src);

        // Build an HMAC signer using a SHA-256 hash
        Signer signer = HMACSigner.newSHA256Signer(SECRET);

        TimeZone tz = TimeZone.getTimeZone(TIMEZONE);

        ZonedDateTime zdt = ZonedDateTime.now(tz.toZoneId()).plusSeconds(EXPIRE_IN);

        // Build a new JWT with an issuer(iss), issued at(iat), subject(sub) and
        // expiration(exp)
        JWT jwt = new JWT().setIssuer(ISSUER).setIssuedAt(ZonedDateTime.now(tz.toZoneId())).setSubject(subject)
                .setExpiration(zdt);

        // Sign and encode the JWT to a JSON string representation
        String encodedJWT = JWT.getEncoder().encode(jwt, signer);

        return encodedJWT;
    }

    /**
     * Valida si el token es valido True si esta vencido, False si esta activo
     *
     * @param encodedJWT
     * @return
     */
    public boolean validateToken(String encodedJWT) {

        boolean result = true;

        JWT jwt = jwt(encodedJWT);
        result = jwt.isExpired();


        return result;
    }

    /**
     * Devuelve el payload del token
     *
     * @param encodedJWT
     * @return
     */
    public String getPayload(String encodedJWT) {
        JWT jwt = jwt(encodedJWT);
        return jwt.subject;
    }

    /**
     * Construye el JWT del string encoded
     *
     * @param encodedJWT
     * @return
     */
    private JWT jwt(String encodedJWT) {

        // Build an HMC verifier using the same secret that was used to sign the JWT
        Verifier verifier = HMACVerifier.newVerifier(SECRET);

        // Verify and decode the encoded string JWT to a rich object
        return JWT.getDecoder().decode(encodedJWT, verifier);
    }

}
