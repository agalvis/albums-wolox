package co.com.wolox.albums.security.service;

import co.com.wolox.albums.security.dto.JwtResponse;
import co.com.wolox.albums.security.jwt.JwtIO;
import co.com.wolox.albums.utils.DateUtils;
import co.com.wolox.albums.utils.GsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Genera el token JWT oauth2.0
 */
@Service
public class AuthService {

  @Value("${albums.security.timezone}")
  private String TIMEZONE;
  @Value("${albums.security.token.expires-in}")
  private int EXPIRE_IN;

  @Autowired
  private JwtIO jwtUtil;
  @Autowired
  private DateUtils dateUtils;
  
  /**
   * Devuelve el token jwt con el payload
   * @param clientId
   * @param src
   * @return
   */
  public JwtResponse getToken(String clientId, Object src) {

    String subject = GsonUtils.serialize(src);

    return generate(clientId, subject);
  }
  
  /**
   * Devuelve el token jwt sin payload
   * @param clientId
   * @return
   */
  public JwtResponse getToken(String clientId) {

    UUID uuid = UUID.randomUUID();

    return generate(clientId, uuid.toString());
  }
  
  private JwtResponse generate(String clientId, Object src) {

    JwtResponse jwt = JwtResponse.builder()
        .tokenType("bearer")
        .accessToken(jwtUtil.generateToken(src))
        .issuedAt(dateUtils.getDateMillis())
        .clientId(clientId)
        .expiresIn(EXPIRE_IN)
        .build();

    return jwt;
  }

}
