package co.com.wolox.albums.security.service;

import co.com.wolox.albums.enums.ErrorEnum;
import co.com.wolox.albums.exceptions.ApiException;
import co.com.wolox.albums.models.Users;
import co.com.wolox.albums.models.repositories.UserRepository;
import co.com.wolox.albums.security.dto.JwtResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

/**
 * Clase para la autenticación de usuarios en el sistema
 */
@Service
public class LoginService {

    @Autowired
    private AuthService authService;

    @Autowired
    private UserRepository userRepository;

    /**
     * Devuelve le JWT si el usuairo autenticado es valido
     *
     * @param clientId
     * @param clientSecret
     * @return
     */
    public JwtResponse login(String clientId, String clientSecret) {

        JwtResponse response = null;

        Users users = userRepository.findByUserName(clientId);
        String pwdHash = BCrypt.hashpw(clientSecret, BCrypt.gensalt());
        if (users != null && BCrypt.checkpw(clientSecret, pwdHash)) {
            response = this.authService.getToken(clientId);
        } else {
            throw new ApiException(ErrorEnum.AUTH_CREDENTIALS_INVALID);
        }
        return response;
    }

}
