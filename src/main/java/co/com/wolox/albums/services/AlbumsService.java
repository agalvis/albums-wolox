package co.com.wolox.albums.services;

import co.com.wolox.albums.clients.AlbumsClientRest;
import co.com.wolox.albums.dto.generated.Albums;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AlbumsService {

    @Autowired
    private AlbumsClientRest albumsClientRest;

    /**
     * consulta los albunes asociados a un usuario
     * @param idUser
     * @return List<Albums>
     */
    public List<Albums> albumsByUserId(Integer idUser) {
        return albumsClientRest.albumsByUserId(idUser).stream().map(p -> new Albums(p.getUserId(), p.getId(), p.getTitle(), p.getAdditionalProperties())).collect(Collectors.toList());
    }
}
