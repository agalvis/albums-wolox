package co.com.wolox.albums.services;

import co.com.wolox.albums.clients.CommentsClientRest;
import co.com.wolox.albums.clients.PostClienteRest;
import co.com.wolox.albums.dto.generated.Comments;
import co.com.wolox.albums.dto.generated.Posts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentsService {

    @Autowired
    private CommentsClientRest commentsClientRest;

    @Autowired
    private PostClienteRest postClienteRest;

    /**
     * Consulta comentarios por el campo name
     * @param name
     * @return List<Comments>
     */
    public List<Comments> commentsByName(String name) {
        return commentsClientRest.commentsByName(name);
    }

    /**
     * Consulta los comentarios asociados a un usuario
     * @param idUser
     * @return List<Comments>
     */
    public List<Comments> commentsByIdUser(Long idUser) {
        List<Comments> response = null;
        List<Posts> postsApi = postClienteRest.postsByIdUser(idUser);
        if (postsApi != null) {
            response = new ArrayList<>();
            for (Posts p : postsApi) {
                response.addAll(commentsClientRest.commentsByPostId(p.getId()));
            }
        }
        return response;
    }
}
