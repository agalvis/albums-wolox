package co.com.wolox.albums.services;

import co.com.wolox.albums.clients.AlbumsClientRest;
import co.com.wolox.albums.clients.PhotosClientRest;
import co.com.wolox.albums.dto.generated.Albums;
import co.com.wolox.albums.dto.generated.Photos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PhotosService {

    @Autowired
    private PhotosClientRest photosClientRest;

    @Autowired
    private AlbumsClientRest albumsClientRest;

    /**
     * Consulta todos las fotos
     *
     * @return List<Photos>
     */
    public List<Photos> findAll() {
        return photosClientRest.photos().stream().map(p -> new Photos(p.getAlbumId(), p.getId(), p.getTitle(), p.getUrl(), p.getThumbnailUrl(), p.getAdditionalProperties())).collect(Collectors.toList());
    }


    /**
     * Consulta todas las fotos asociadas a un usuario
     *
     * @param userId
     * @return List<Photos>
     */
    public List<Photos> photosByUserId(Integer userId) {
        List<Photos> response = new ArrayList<>();
        List<Albums> albums = albumsClientRest.albumsByUserId(userId).stream().map(a -> new Albums(a.getId())
        ).collect(Collectors.toList());
        for (Albums a : albums) {
            response.addAll(photosClientRest.photosByAlbumId(a.getId()).stream().map(p -> new Photos(p.getAlbumId(), p.getId(), p.getTitle(), p.getUrl(), p.getThumbnailUrl(), p.getAdditionalProperties())).collect(Collectors.toList()));
        }
        return response;
    }
}
