package co.com.wolox.albums.services;

import co.com.wolox.albums.clients.AlbumsClientRest;
import co.com.wolox.albums.clients.UserClientRest;
import co.com.wolox.albums.dto.ShareReq;
import co.com.wolox.albums.dto.ShareRes;
import co.com.wolox.albums.dto.generated.Albums;
import co.com.wolox.albums.dto.generated.Users;
import co.com.wolox.albums.enums.ErrorEnum;
import co.com.wolox.albums.enums.Permission;
import co.com.wolox.albums.exceptions.ApiException;
import co.com.wolox.albums.models.Share;
import co.com.wolox.albums.models.repositories.ShareRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShareService {

    @Autowired
    private ShareRepository shareRepository;

    @Autowired
    private UserClientRest userClientRest;

    @Autowired
    private AlbumsClientRest albumsClientRest;

    /**
     * Persiste en BD el albun compartido a un usuario con un permiso definido,
     * valida que el album no este creado
     * @param shareDTO
     * @return ShareRes
     */
    public ShareRes save(ShareReq shareDTO) {
        ShareRes response = null;
        try {
            Share shareUnique = shareRepository.findByIdUserAndIdAlbum(shareDTO.getIdUser(), shareDTO.getIdAlbum());
            if (shareUnique == null) {
                Users users = userClientRest.usersByUserId(shareDTO.getIdUser().intValue());
                Albums album = albumsClientRest.albumsById(shareDTO.getIdAlbum().intValue());
                if (users != null && album != null) {
                    Share share = new Share(shareDTO.getIdUser(), shareDTO.getIdAlbum(), Permission.valueOf(shareDTO.getPermission()));
                    Share shareDB = shareRepository.save(share);
                    if (shareDB != null) {
                        response = new ShareRes(shareDB.getId(), users, album, shareDB.getPermission());
                    }
                } else {
                    throw new ApiException(ErrorEnum.BAD_REQUEST_SHARE);
                }
            } else {
                throw new ApiException(ErrorEnum.BAD_REQUEST_UNIQUE);
            }

        } catch (IllegalArgumentException | FeignException e) {
            throw new ApiException(ErrorEnum.BAD_REQUEST_SHARE);
        }
        return response;
    }

    /**
     * Cambiar los permisos de un usuario para un álbum determinado.
     * valida que el permiso ya este creado y modifica el permiso
     * @param shareDTO
     * @return ShareRes
     */
    public ShareRes update(ShareReq shareDTO) {
        ShareRes response = null;
        try {
            Share shareUnique = shareRepository.findByIdUserAndIdAlbum(shareDTO.getIdUser(), shareDTO.getIdAlbum());
            if (shareUnique != null) {
                Users users = userClientRest.usersByUserId(shareDTO.getIdUser().intValue());
                Albums album = albumsClientRest.albumsById(shareDTO.getIdAlbum().intValue());
                if (users != null && album != null) {
                    if (shareUnique != null) {
                        shareUnique.setPermission(Permission.valueOf(shareDTO.getPermission()));
                        Share shareDB = shareRepository.save(shareUnique);
                        if (shareDB != null) {
                            response = new ShareRes(shareDB.getId(), users, album, shareDB.getPermission());
                        }
                    } else {
                        throw new ApiException(ErrorEnum.BAD_REQUEST);
                    }
                } else {
                    throw new ApiException(ErrorEnum.BAD_REQUEST_SHARE);
                }
            } else {
                throw new ApiException(ErrorEnum.BAD_REQUEST_NOT_REGISTERED);
            }
        } catch (IllegalArgumentException | FeignException e) {
            throw new ApiException(ErrorEnum.BAD_REQUEST_SHARE);
        }
        return response;
    }
}
