package co.com.wolox.albums.services;

import co.com.wolox.albums.clients.AlbumsClientRest;
import co.com.wolox.albums.clients.UserClientRest;
import co.com.wolox.albums.dto.UsersShareRes;
import co.com.wolox.albums.dto.generated.Albums;
import co.com.wolox.albums.dto.generated.Users;
import co.com.wolox.albums.enums.Permission;
import co.com.wolox.albums.models.Share;
import co.com.wolox.albums.models.repositories.ShareRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserClientRest userClientRest;

    @Autowired
    private ShareRepository shareRepository;

    @Autowired
    private AlbumsClientRest albumsClientRest;

    /**
     * Retorna todos los usuarios de la API
     *
     * @return List<Users>
     */
    public List<Users> findAll() {
        return userClientRest.users().stream().map(p -> new Users(p.getId(), p.getName(), p.getUsername(), p.getEmail(), p.getAddress(), p.getPhone(), p.getWebsite(), p.getCompany(), p.getAdditionalProperties())).collect(Collectors.toList());
    }

    /**
     * Retorna todos los usuarios que tienen un permiso determinado respecto a un
     * álbum específico.
     *
     * @param permission
     * @param idAlbum
     * @return UsersShareRes
     */
    public UsersShareRes usersByPermissionAlbum(Permission permission, Long idAlbum) {
        UsersShareRes response = null;
        List<Share> shares = shareRepository.findByIdAlbumAndPermission(idAlbum, permission);
        if (shares != null && !shares.isEmpty()) {
            Albums album = albumsClientRest.albumsById(idAlbum.intValue());
            List<Users> users = new ArrayList<>();
            for (Share u : shares) {
                Users user = userClientRest.usersByUserId(u.getIdUser().intValue());
                if (user != null) {
                    users.add(user);
                }
            }
            response = new UsersShareRes(album, permission, users);
        }
        return response;
    }

}
