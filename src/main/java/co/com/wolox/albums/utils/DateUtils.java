package co.com.wolox.albums.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Utilidad para el manejo de fechas
 *
 */
@Component
public class DateUtils {

	@Value("${albums.security.timezone}")
	private String TIMEZONE;

	private SimpleDateFormat simpleDateFormat() {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
		return sdf;
	}

	/**
	 * Devuelve la fecha en string
	 * @return
	 */
	public String getDateString() {

		Date now = new Date();
		return simpleDateFormat().format(now);
	}

	/**
	 * Devuelve la fecha en timemillis
	 * @return
	 */
	public long getDateMillis() {

		Date now = new Date();
		String strDate = simpleDateFormat().format(now);
		Date strNow = new Date();
		
		try {
			strNow = simpleDateFormat().parse(strDate);
		} catch (ParseException e) { }

		return strNow.getTime();
	}

}
